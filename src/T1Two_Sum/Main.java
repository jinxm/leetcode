package T1Two_Sum;

/**
 * @author: jxm
 * @description:
 * @date: 2018/10/12 上午9:41
 */
public class Main {

    public static void main(String[] args) {
        int[] nums = {3, 2, 4};
        int[] returnNums = new Solution().twoSum(nums, 6);
        for (int returnNum : returnNums) {
            System.out.print(returnNum + ", ");
        }
    }
}
