package T2Add_Two_Numbers;

/**
 * @author: jxm
 * @description:
 * @date: 2018/10/14 下午2:41
 */
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode resultLN = new ListNode(0);
        ListNode resultLN1 = resultLN;
        int result = 0;
        // 上一位进位标识
        boolean flag = false;
        while (null != l1 || null != l2) {
            result = l1.val + l2.val;
            System.out.print(result + "->");

            resultLN.val = (l1.val + l2.val + (flag ? 1 : 0));
            flag = resultLN.val >= 10;
            if (flag) {
                resultLN.val = resultLN.val % 10;
            }
            if (l1.next == null && l2.next == null && !flag) {
                break;
            }
            if (l1.next == null) {
                l1.next = new ListNode(0);
            }
            if (l2.next == null) {
                l2.next = new ListNode(0);
            }
            resultLN.next = new ListNode(0);

            l2 = l2.next;
            l1 = l1.next;

            resultLN = resultLN.next;
        }
        resultLN.next = null;
        return resultLN1;
    }
}
