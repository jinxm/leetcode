package T2Add_Two_Numbers;


/**
 * @author: jxm
 * @description:
 * @date: 2018/10/12 上午9:41
 */
public class Main {

    public static void main(String[] args) {
        int[] l1Arr = {1, 8};
        int[] l2Arr = {0};
        ListNode l1 = new ListNode(0);
        ListNode l1Copy = l1;

        ListNode l2 = new ListNode(0);
        ListNode l2Copy = l2;
        for (int i = 0; i < l1Arr.length; i++) {
            l1.val = l1Arr[i];
            if (i + 1 == l1Arr.length) {
                break;
            }
            l1.next = new ListNode(0);
            l1 = l1.next;
        }

        for (int i = 0; i < l2Arr.length; i++) {
            l2.val = l2Arr[i];
            if (i + 1 == l2Arr.length) {
                break;
            }
            l2.next = new ListNode(0);
            l2 = l2.next;
        }

        ListNode result = new Solution().addTwoNumbers(l1Copy, l2Copy);
        System.out.println(ListNode.toString(result));
    }
}
