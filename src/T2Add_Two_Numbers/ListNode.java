package T2Add_Two_Numbers;

/**
 * @author: jxm
 * @description:
 * @date: 2018/10/14 下午2:39
 */
class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }

    public static String toString(ListNode listNode) {
        StringBuilder result = new StringBuilder("\n");
        while (listNode != null) {
            result.append(listNode.val).append(" -> ");
            listNode = listNode.next;
        }
        return result.toString();
    }
}
